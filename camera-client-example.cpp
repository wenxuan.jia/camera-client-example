
//extern "C" {
//    #include <gst/gst.h>
//	#include <gst/app/gstappsink.h>
//    #include <gst/video/video-frame.h>
//}

#include <gst/gst.h>
#include <gst/app/gstappsink.h>
#include <gst/video/video-frame.h>


//#include <opencv2/opencv.hpp>


struct gstreamer_data {
	gulong signal_handler_id;

	GstElement* udp_source;
	GstElement* rtp_decoder;
	GstElement* video_decoder;
	GstElement* video_converter;
	GstElement* app_sink;

	GstElement* pipeline;

	GMainLoop* main_loop;
};


static GstFlowReturn new_sample_callback(GstElement* appsink, gpointer udata);
static GstPadProbeReturn udp_source_buffer_pad_probe_callback(GstPad* pad, GstPadProbeInfo* info, gstreamer_data* user_data);
static void udp_source_timeout_callback(GstBus* bus, GstMessage* message, gstreamer_data* user_data);
gboolean bus_error_callback(GstBus* bus, GstMessage* message, gpointer user_data);

static GstFlowReturn new_sample_callback(GstElement* appsink, gpointer udata) {
    (void) udata;

	GstSample* sample;
    g_signal_emit_by_name(appsink, "pull-sample", &sample);
	if (sample) {
		GstCaps* caps = gst_sample_get_caps(sample);
		if (caps) {
            GstBuffer* buffer = gst_sample_get_buffer(sample);
            if (buffer) {
	            GstMapInfo map_info;
                if (gst_buffer_map(buffer, &map_info, GST_MAP_READ)) {
                    GstVideoInfo* video_info = gst_video_info_new();
                    if (gst_video_info_from_caps(video_info, caps)) {
                        g_print("%s %i %i\n", video_info->finfo->name, GST_VIDEO_INFO_WIDTH(video_info), GST_VIDEO_INFO_HEIGHT(video_info));

                        //cv::Mat frame = cv::Mat(cv::Size(GST_VIDEO_INFO_WIDTH(video_info), GST_VIDEO_INFO_HEIGHT(video_info)),
                        //CV_8U, (char*) map_info.data, cv::Mat::AUTO_STEP);
                    }
					gst_buffer_unmap(buffer, &map_info);
                }
            }
		}
		gst_sample_unref(sample);
	}

	return GST_FLOW_OK;
}

static GstPadProbeReturn udp_source_buffer_pad_probe_callback(GstPad* pad, GstPadProbeInfo* info, gstreamer_data* user_data) {
	(void) pad;
	(void) info;
	GstBus* bus;


	g_print("have data\n");

	bus = gst_element_get_bus(user_data->pipeline);
	user_data->signal_handler_id = g_signal_connect(G_OBJECT(bus), "message::element", (GCallback) udp_source_timeout_callback, user_data);
	gst_object_unref(bus);

	return GST_PAD_PROBE_REMOVE;
}

static void udp_source_timeout_callback(GstBus* bus, GstMessage* message, gstreamer_data* user_data) {
	const GstStructure* st = gst_message_get_structure(message);
	GstPad* pad;

	if (GST_MESSAGE_TYPE(message) == GST_MESSAGE_ELEMENT) {
		if (gst_structure_has_name(st, "GstUDPSrcTimeout")) {
			g_print("no data\n");

			g_signal_handler_disconnect(G_OBJECT(bus), user_data->signal_handler_id);

			pad = gst_element_get_static_pad(user_data->udp_source, "src");
			gst_pad_add_probe(pad, GST_PAD_PROBE_TYPE_BUFFER, (GstPadProbeCallback) udp_source_buffer_pad_probe_callback, user_data, NULL);
			gst_object_unref(pad);
		}
	}
}

gboolean bus_error_callback(GstBus* bus, GstMessage* message, gpointer user_data) {
    (void) bus;
    GMainLoop* main_loop = ((gstreamer_data*) user_data)->main_loop;

    switch (GST_MESSAGE_TYPE(message)) {
        case GST_MESSAGE_EOS: {
            g_print("End of stream\n");
            g_main_loop_quit(main_loop);
            break;
        }
        case GST_MESSAGE_ERROR: {
            GError *error;
            gchar *debug;
            gst_message_parse_error(message, &error, &debug);
            g_printerr("Error received from element %s: %s\n", GST_OBJECT_NAME(message->src), error->message);
            g_error_free(error);
            g_printerr("Debugging information: %s\n", debug ? debug : "none");
            g_free(debug);
            g_main_loop_quit(main_loop);
            break;
        }
        case GST_MESSAGE_WARNING: {
            GError *error;
            gchar *debug;
            gst_message_parse_warning(message, &error, &debug);
            g_printerr("Warning received from element %s: %s\n", GST_OBJECT_NAME(message->src), error->message);
            g_error_free(error);
            g_printerr("Debugging information: %s\n", debug ? debug : "none");
            g_free(debug);
            break;
        }
        default:
            g_print("%s %s\n", GST_MESSAGE_SRC_NAME(message), GST_MESSAGE_TYPE_NAME(message));
            break;
        }

    return TRUE;
}


int main() {
	char const* address = "239.192.106.42";
	gint port = 5004;
	guint64 udp_timeout = 1E9;

	gst_init(NULL, NULL);

	gstreamer_data data;

	data.udp_source = gst_element_factory_make("udpsrc", "udp_source");
	g_object_set(G_OBJECT(data.udp_source),
		"address", address,
		"port", port,
		"caps", gst_caps_new_simple("application/x-rtp", "payload", G_TYPE_INT, 127, NULL),
		"timeout", udp_timeout,
		NULL);
    if (!data.udp_source) {
        g_printerr("Failed to create element 'udp_src'\n");
        return -1;
    }

	data.rtp_decoder = gst_element_factory_make("rtph264depay", "rtp_decoder");
    if (!data.rtp_decoder) {
        g_printerr("Failed to create element 'rtp_decoder'\n");
        return -1;
    }

	data.video_decoder = gst_element_factory_make("avdec_h264", "video_decoder");
    if (!data.video_decoder) {
        g_printerr("Failed to create element 'video_decoder'\n");
        return -1;
    }

	data.video_converter = gst_element_factory_make("videoconvert", "video_converter");
    if (!data.video_converter) {
        g_printerr("Failed to create element 'video_converter'\n");
        return -1;
    }

	data.app_sink = gst_element_factory_make("appsink", "app_sink");
	g_object_set(G_OBJECT(data.app_sink),
		"emit-signals", true,
        "max-buffers", G_GUINT64_CONSTANT(1),
		"drop", true,
		"sync", false,
		"caps", gst_caps_new_simple("video/x-raw", "format", G_TYPE_STRING, "GRAY8", NULL),
		NULL);
    if (!data.app_sink) {
        g_printerr("Failed to create element 'app_sink'\n");
        return -1;
    }

	g_signal_connect(data.app_sink, "new-sample", (GCallback) new_sample_callback, &data);

    data.pipeline = gst_pipeline_new("pipeline");
    if (!data.pipeline) {
        g_printerr("Failed to create pipeline\n");
        return -1;
    }

	gst_bin_add_many(
		GST_BIN(data.pipeline),
		data.udp_source,
		data.rtp_decoder,
		data.video_decoder,
		data.video_converter,
		data.app_sink,
		NULL);


	if (gst_element_link_many(
		data.udp_source,
		data.rtp_decoder,
		data.video_decoder,
		data.video_converter,
		data.app_sink,
		NULL) != TRUE)
		{
			g_printerr("Elements could not be linked.\n");
			gst_object_unref(data.pipeline);
			return -1;
		}


    data.main_loop = g_main_loop_new(NULL, FALSE);

	GstBus* bus = gst_element_get_bus(data.pipeline);
	gst_bus_add_signal_watch(bus);
	g_signal_connect(G_OBJECT(bus), "message::error", (GCallback) bus_error_callback, &data);
	data.signal_handler_id = g_signal_connect(G_OBJECT(bus), "message::element", (GCallback) udp_source_timeout_callback, &data);
	gst_object_unref(bus);

    GstStateChangeReturn ret = gst_element_set_state(data.pipeline, GST_STATE_PLAYING);
    if (ret == GST_STATE_CHANGE_FAILURE) {
        g_printerr("Unable to set the pipeline to the playing state.\n");
        gst_object_unref(data.pipeline);
        g_main_loop_unref(data.main_loop);
        return -1;
    }

    g_print("Starting main loop.\n");
    g_main_loop_run(data.main_loop);

    gst_element_set_state(data.pipeline, GST_STATE_NULL);
    gst_object_unref(data.pipeline);
    g_main_loop_unref(data.main_loop);

    return 0;
}
