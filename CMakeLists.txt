cmake_minimum_required(VERSION 3.18)

project(camera-client-example)


find_package(PkgConfig)
pkg_search_module(GST REQUIRED
    gstreamer-1.0,
    gstreamer-app-1.0,
    gstreamer-video-1.0
)

#find_package(OpenCV REQUIRED)

add_executable(camera-client-example camera-client-example.cpp)

#target_include_directories(camera-client-example PRIVATE ${GST_INCLUDE_DIRS} ${OpenCV_INCLUDE_DIRS})
#target_link_libraries(camera-client-example PRIVATE ${GST_LIBRARIES} ${OpenCV_LIBS})


target_include_directories(camera-client-example PRIVATE ${GST_INCLUDE_DIRS})
target_link_libraries(camera-client-example PRIVATE ${GST_LIBRARIES})


install(TARGETS camera-client-example DESTINATION bin)
